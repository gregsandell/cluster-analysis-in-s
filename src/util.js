class Util {
  constructor (sdata) {
    this.sdata = sdata
    //    this.generateEmptyMatrix()
  }

  generateEmptyMatrix () {
    this.matrix = []
    for (let i = 0; i < this.sdata.length; i++) {
      this.matrix.push(new Array(this.sdata.length))
    }
  }

  getMatrix () {
    return this.matrix
  }

  scoreSinglePair (data1, data2) {
    const [rank1, category1, strength1] = data1.split(':'),
      [rank2, category2, strength2] = data2.split(':')

    if (Number(category1) === Number(category2)) {
      const big = Number(strength1) === Number(strength2) ? 10 : 5
      return big - Math.abs(Number(rank1) - Number(rank2))
    }
    return 0
  }

  scoreMultiPairs (sdata1, sdata2) {
    const points1 = sdata1.split('%'),
      points2 = sdata2.split('%')
    let score = 0

    for (let i = 0; i < 5; i++) {
      for (let j = 0; j < 5; j++) {
        const s = this.scoreSinglePair(points1[i], points2[j])
        score += s
        //        console.log(`Score of ${s} for [${points1[i]}] and [${points2[j]}] for new score of ${score}`)
      }
    }
    return score
  }

  scoreFullMatrix () {
    // harvey/haley, harvey/jenny,  harvey/john
    let sbuff = []
    sbuff.push('')
    for (let i = 0; i < this.sdata.length; i++) {
      sbuff.push('"' + this.sdata[i].firstname + '"')
    }
    console.log(sbuff.join(','))
    for (let i = 0; i < this.sdata.length; i++) {
      sbuff = []
      sbuff.push('"' + this.sdata[i].firstname + '"')
      for (let j = 0; j < this.sdata.length; j++) {
        const score = i === j ? 0 : (100 - this.scoreMultiPairs(this.sdata[i].data, this.sdata[j].data))
        sbuff.push(score)
      }
      console.log(sbuff.join(','))
    }
  }
}
module.exports = Util
