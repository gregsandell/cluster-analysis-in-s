const sdata = [
  {
    firstname: 'Harvey',
    lastname: 'Bierman',
    data: '0:1:0%1:0:7%2:0:1%3:3:7%4:2:8'
  },
  {
    firstname: 'Haley',
    lastname: 'Nemann',
    data: '0:2:7%1:3:2%2:3:7%3:2:8%4:0:1'
  },
  {
    firstname: 'Jenny',
    lastname: 'Hanlon',
    data: '0:2:1%1:2:8%2:3:6%3:0:8%4:3:3'
  },
  {
    firstname: 'John',
    lastname: 'Visser',
    data: '0:0:0%1:0:4%2:3:0%3:3:6%4:3:1'
  },
  {
    firstname: 'JessicaT',
    lastname: 'Tayts',
    data: '0:3:1%1:1:7%2:2:6%3:3:4%4:1:2'
  },
  {
    firstname: 'Kendall',
    lastname: 'Gysin',
    data: '0:2:8%1:3:7%2:1:3%3:0:1%4:1:2'
  },
  {
    firstname: 'MichelleC',
    lastname: 'Connors',
    data: '0:0:7%1:30%2:0:5%3:1:2%4:2:4'
  },
  {
    firstname: 'MichelleH',
    lastname: 'Hinkle',
    data: '0:0:8%1:2:4%2:0:7%3:0:3%4:0:2'
  },
  {
    firstname: 'Nick',
    lastname: 'Kuhnel',
    data: '0:3:2%1:3:3%2:3:0%3:3:5%4:3:7'
  },
  {
    firstname: 'Jordan',
    lastname: 'Stephenson',
    data: '0:2:0%1:0:8%2:2:5%3:0:1%4:1:6'
  },
  {
    firstname: 'Mike',
    lastname: 'Harsh',
    data: '0:0:4%1:3:6%2:3:5%3:0:0%4:3:0'
  },
  {
    firstname: 'Joel',
    lastname: 'Cardinal',
    data: '0:3:6%1:3:5%2:3:4%3:0:0%4:2:0'
  },
  {
    firstname: 'Greg',
    lastname: 'Sandell',
    data: '0:0:4%1:3:6%2:3:5%3:3:7%4:0:0'
  },
  {
    firstname: 'Scott',
    lastname: 'Deitz',
    data: '0:3:6%1:2:6%2:0:0%3:3:5%4:3:4'
  },
  {
    firstname: 'JessicaB',
    lastname: 'Brooks',
    data: '0:0:0%1:2:8%2:3:6%3:1:1%4:3:0'
  },
  {
    firstname: 'Daniel',
    lastname: 'Rodriguez',
    data: '0:3:2%1:2:8%2:3:3%3:2:3%4:3:7'
  },
  {
    firstname: 'Emile',
    lastname: 'van Hanegem',
    data: '0:0:4%1:0:3%2:2:4%3:3:0%4:2:0'
  },
  {
    firstname: 'Jodi',
    lastname: 'Geditz',
    data: '0:3:5%1:3:0%2:0:4%3:0:0%4:0:5'
  },
  {
    firstname: 'Nate',
    lastname: 'Harnack',
    data: '0:2:1%1:3:7%2:3:3%3:2:2%4:0:7'
  },
  {
    firstname: 'Jonathan',
    lastname: 'Long',
    data: '0:1:7%1:1:2%2:0:7%3:3:4%4:1:4'
  },
  {
    firstname: 'Randall',
    lastname: 'Kinsel',
    data: '0:3:7%1:3:5%2:3:3%3:3:4%4:3:6'
  },
  {
    firstname: 'Heather',
    lastname: 'Pietro',
    data: '0:2:0%1:0:8%2:3:4%3:2:5%4:0:0'
  },
  {
    firstname: 'Lisa',
    lastname: 'Russell',
    data: '0:2:3%1:2:4%2:2:2%3:0:3%4:1:2'
  }
]
/*
  Scoring

  1. n points for each item sharing category
  3. k points for degree of rank similarity for same item in category
     0:0, 1:1, 2:2, 3:3, 4:4   exact same: 10  (6 - (x-y))
     0:1, 1:2, 2:3, 3:4        one away: 9
     0:2, 1:3, 2:4             two away: 8
     0:3, 1:4                  three away: 7
     0:4                       four away 6
  3. p points for different item in same category
     0:0, 1:1, 2:2, 3:3, 4:4   exact same: 5
     0:1, 1:2, 2:3, 3:4        one away: 4
     0:2, 1:3, 2:4             two away: 3
     0:3, 1:4                  three away: 2
     0:4                       four away 1

     same category only        1
 */
const model = [
  {
    category: 'executing',
    strength: [
      //  0
      'achiever', 'arranger', 'belief', 'consistency', 'deliberative', 'discipline', 'focus', 'responsibility', 'restorative']
    //      0          1          2        3             4              5            6       7                 8
  },
  {
    category: 'influencing',
    strength: [
      //  1
      'activator', 'command', 'communication', 'competition', 'maximizer', 'self-assurance', 'significance', 'woo'
      //      0           1         2               3             4           5                6              7
    ]
  },
  {
    category: 'relationship building',
    strength: [
      //  2
      'adaptability', 'connectedness', 'developer', 'empathy', 'harmony', 'includer', 'individualization', 'positivity', 'relator'
      //    0               1                2            3          4          5           6                    7             8
    ]
  },
  {
    category: 'strategic thinking',
    strength: [
      //  3
      'analytical', 'context', 'futuristic', 'ideation', 'input', 'intellection', 'learner', 'strategic'
      //      0            1         2            3          4       5              6         7
    ]
  }
]
module.exports = { sdata, model }
